<?php

namespace unit\services;

use app\fakes\FakeCommentRepository;
use app\fakes\FakeEmailNotifyService;
use app\fakes\FakeUserRepository;
use app\interfaces\CommentRepositoryInterface;
use app\models\Comment;
use app\models\User;
use app\repositories\CommentRepository;
use app\services\CommentService;
use DomainException;

class CommentServiceTest extends \Codeception\Test\Unit
{
    /**
     * @dataProvider providerCreate
     */
    public function testCreate($commentText, $exception, $userId)
    {
        $user = new User();
        $comments = new FakeCommentRepository([]);
        $users = new FakeUserRepository($user);
        $notify = new FakeEmailNotifyService();
        $service = new CommentService($comments, $notify, $users);
        $comment = new Comment();
        $comment->comment = $commentText;
        if ($exception) {
            $this->expectException($exception);
        }
        $service->create($comment, $userId);
        $this->assertNotNull($comment);

    }
    /*public function testCreateMock()
    {
        $user = new User();
        $comments = new FakeCommentRepository([]);
        $comments = $this->createMock(CommentRepositoryInterface::class);
        $comments->method('getCommentsWithReplies')->willReturn('123');
        $this->assertEquals('123',$comments->getCommentsWithReplies());
        $comments->expects($this->never())->method('save');
        $users = new FakeUserRepository($user);
        $notify = new FakeEmailNotifyService();
        $service = new CommentService($comments,$notify,$users);
        $comment = new Comment();
        $comment->comment = 'Набор букв.';
        /*if($exception){
            $this->expectException($exception);
        }
        $service->create($comment,100);
        $this->assertNotNull($comment);

    }*/
    /**
     * @dataProvider providerUpdate
     */
    public function testUpdate($commentText, $expectedException, $userId, $user)
    {
        if ($user) {
            $user = new User();
        } else {
            $user = null;
        }
        $comments = new FakeCommentRepository([]);
        $users = new FakeUserRepository($user);
        $notify = new FakeEmailNotifyService();
        $service = new CommentService($comments, $notify, $users);
        $comment = new Comment();
        $comment->comment = $commentText;
        $comment->author_id = 100;
        if ($expectedException) {
            $this->expectException($expectedException);
        }
        $service->update($comment, $userId);
        $this->assertNotNull($comment);
    }

    /**
     * @dataProvider providerDelete
     */
    public function testDelete($commentText, $expectedException, $userId, $user)
    {
        if ($user) {
            $user = new User();
        } else {
            $user = null;
        }
        $comments = new FakeCommentRepository([]);
        $users = new FakeUserRepository($user);
        $notify = new FakeEmailNotifyService();
        $service = new CommentService($comments, $notify, $users);
        $comment = new Comment();
        $comment->comment = $commentText;
        $comment->author_id = 100;
        if ($expectedException) {
            $this->expectException($expectedException);
        }
        $service->delete($comment, $userId);
        $this->assertNotNull($comment);

    }

    public function providerDelete()
    {
        return [
            [
                'commentText' => 'Hello world!',
                'expectedException' => null,
                'userId' => 100,
                'user' => 100
            ],
            [
                'commentText' => '',
                'expectedException' => DomainException::class,
                'userId' => 100,
                'user' => 100
            ]
        ];
    }

    public function providerCreate()
    {
        return [
            [
                'commentText' => '01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890',
                'expectedException' => DomainException::class,
                'userId' => 100

            ],
            /*[
                'commentText' => '',
                'exception' => DomainException::class,
                'userId'=> 100

            ],
            [
                'commentText' => '123',
                'expectedException' => null,
                'userId'=>100

            ]*/

        ];
    }

    public function providerUpdate()
    {
        return [
            [
                'commentText' => '01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890',
                'expectedException' => DomainException::class,
                'userId' => 100,
                'user' => null

            ],
            /*[
                'commentText' => '111',
                'expectedException' => null,
                'userId' => 100,
                'user'=> true

            ],
           [
                'commentText' => '111',
                'expectedException' => DomainException::class,
                'userId' => 102,
                'user'=> true

            ],
            [
                'commentText' => '',
                'expectedException' => DomainException::class,
                'userId' => 100,
                'user'=> true
            ]*/

        ];
    }


    /*public function testDelete()
    {
        $repository = new FakeCommentRepository([]);
        $service = new CommentService($repository);
        $comment = new Comment();
        $service->delete($comment,101);
        $this->assertNotNull($comment);
        $this->expectException();
    }*/
}