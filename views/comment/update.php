<?php
/**
 * @var $model app\models\Comment
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;


$form = ActiveForm::begin(); ?>

<?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

<div class="form-group">
            <div class="offset-lg-1 col-lg-11">
                <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>
            </div>
</div>
<?php ActiveForm::end(); ?>




