<?php

use app\helpers\DeclensionHelper;
use app\models\Comment;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use app\repositories\CommentRepository;

/**
 * @var \yii\web\View $this
 * @var Comment[] $comments
 */

$this->registerCssFile('/css/comment.css');
?>


<?php foreach($comments as $comment): ?>

    <p xmlns="http://www.w3.org/1999/html"> <b>Комментарий:</b> </p><br>



    <div style="background-color: rgba(196,252,214,0.65);"><b><?= $comment['comment']['author'] ?></b>&nbsp<?= $comment['comment']['date'] ?></div>



    <?php if ((!Yii::$app->user->isGuest) && (Yii::$app->user->identity->username) === $comment['comment']['author']) : ?>

        <div class="deleteIcon" onclick="myFunction(<?= $comment['comment']['id'] ?>)" style=" float: right;">

            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#3374FF" class="bi bi-trash-fill"
                 viewBox="0 0 16 16">
                <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
            </svg>
        </div>

        <div>
            <a href="<?= Yii::$app->urlManager->createUrl(['/comment/update', 'id' => $comment['comment']['id']]) ?>"
               style="float: right;">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                     class="bi bi-pencil-fill" viewBox="0 0 16 16">
                    <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                </svg>
            </a>
        </div>

    <?php endif;?>

    <div><?= $comment['comment']['comment'] ?></div>

    <div><font color="#d3d3d3"> <?= DeclensionHelper::declension(Comment::getDateDiff($comment['comment']['date']),
                (Comment::getArrTime(Comment::getDateDiff($comment['comment']['date'])))) ?></font></div>

    <div class="reply">  <a href="reply?id=<?= $comment['comment']['id'] ?>">Ответить</a> </div> <br><br>

    <div style="margin-left: 5%" ><p> <b>Ответы:</b> </p> </div>


    <?php for ($i = 0; $i < count($comment['replies']);$i++) :?>

        <div style="background-color: rgba(183,224,225,0.65);margin-left: 5%"><b><?= $comment['replies'][$i]['author'] ?></b>&nbsp<?= $comment['replies'][$i]['date'] ?></div>

        <div style="margin-left: 5%"><?= $comment['replies'][$i]['comment'] ?></div>

        <div style="margin-left: 5%"><font color="#d3d3d3"> <?= DeclensionHelper::declension(Comment::getDateDiff($comment['replies'][$i]['date']),
                    (Comment::getArrTime(Comment::getDateDiff($comment['replies'][$i]['date'])))) ?></font></div><br>

    <?php endfor ?>

    <?php if(!next($comments)):?>

        <div hidden><?= $lastIndex = $comment['comment']['id']?></div>

    <?php endif;?>

<?php endforeach; ?>

<style>
    /*.addIcon{*/
    /*   float: left;*/
    /*   }*/

    .reply{
        position: absolute;
        left: 500px;
    }

    .show{
        position: absolute;
        right: 500px;
    }
</style>

<?php if(!Yii::$app->user->isGuest): ?>

    <div class="addIcon" >
        <a href="create">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill=#3374FF class="bi bi-chat-left-dots-fill" viewBox="0 0 16 16">
                <path d="M0 2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H4.414a1 1 0 0 0-.707.293L.854 15.146A.5.5 0 0 1 0 14.793V2zm5 4a1 1 0 1 0-2 0 1 1 0 0 0 2 0zm4 0a1 1 0 1 0-2 0 1 1 0 0 0 2 0zm3 1a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
            </svg>
        </a>
    </div><br>

<?php endif;?>

<script>
    function myFunction(id) {

        if(confirm("Вы уверены,что хотите удалить комментарий?") === true){
            document.location.href = "delete?id="+id;
        }else {
            document.location.href = "index";
        }
    }
</script>



