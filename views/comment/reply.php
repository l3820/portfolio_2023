<?php

/**
* @var $comment app\models\Comment
 * @var $reply app\models\Comment
*/

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

?>

<p>Комментарий:</p>
<style>
    .comment{
        border: solid 1px lightgray;
    }
</style>
<div class="comment"><?= $comment->comment ?></div><br>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($reply, 'comment')->textarea(['rows' => 6])->label('Ответ:')?>


<div class="form-group">

    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>

</div>

<?php ActiveForm::end(); ?>
