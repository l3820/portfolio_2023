<?php

/* @var $model app\models\Comment */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;


?>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

<div class="form-group">

    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>

</div>

<?php ActiveForm::end(); ?>
