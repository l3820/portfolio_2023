<?php

use app\helpers\DeclensionHelper;
use app\models\Comment;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use app\repositories\CommentRepository;

/**
 * @var \yii\web\View $this
 * @var Comment $comment
 * @var Comment[] $replies
 */
?>

<!--<div class="comment" data-id="<?//$comment['id'] ?>">
    <div style="background-color: rgba(196,252,214,0.65);">
        <b><?// $comment['author'] ?></b>&nbsp<?// $comment['date'] ?></div>
    <div><?// $comment['comment'] ?></div>
    <br>
</div>-->

<div class="comment" data-id="<?= $comment->id ?>">

    <div style="background-color: rgba(196,252,214,0.65);">
        <b><?= $comment->author ?></b>&nbsp<?= $comment->date ?></div>

    <?php if ((!Yii::$app->user->isGuest) && (Yii::$app->user->identity->username) === $comment->author) : ?>

        <div class="icon">
            <div class="deleteIcon" onclick="deleteFunction(<?= $comment->id ?>)"
                 style=" float: right;">

                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#3374FF"
                     class="bi bi-trash-fill"
                     viewBox="0 0 16 16">
                    <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
                </svg>
            </div>

            <div>
                <a href="<?= Yii::$app->urlManager->createUrl(['/comment/update', 'id' => $comment->id]) ?>"
                   style="float: right;">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                         class="bi bi-pencil-fill" viewBox="0 0 16 16">
                        <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                    </svg>
                </a>
            </div>
        </div>

    <?php endif; ?>

    <div><?= $comment->comment ?></div>

    <div style="color:#d3d3d3 ">
        <?php $dateDiff = Comment::getDateDiff($comment->date); ?>
        <?= DeclensionHelper::declension(($dateDiff), Comment::getArrTime($dateDiff)) ?>
    </div>
    <br>

    <div class="reply" style="position: absolute;left: 500px;">
        <a href="<?= Yii::$app->urlManager->createUrl(['/comment/reply', 'id' => $comment->id]) ?>">Ответить</a>
    </div><br><br>

    <?php foreach ($replies as $reply): ?>

        <div class="reply">

            <div style="background-color: rgba(183,224,225,0.65);margin-left: 5%">
                <b><?= $reply->author ?></b>&nbsp<?= $reply->date ?></div>

            <div style="margin-left: 5%"><?= $reply->comment ?></div>

            <div style="margin-left: 5%; color:#d3d3d3 ">
                <?php $dateDiffReplies = Comment::getDateDiff($reply->date) ?>
                <?= DeclensionHelper::declension($dateDiffReplies, Comment::getArrTime($dateDiffReplies)) ?>
            </div>
            <br>

        </div>

    <?php endforeach; ?>

</div>

<?php
//var_dump($arr);









