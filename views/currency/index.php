<?php

use app\models\SearchCurrency;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\db\Query;

/**
 * @var ActiveDataProvider $dataProvider
 * @var SearchCurrency $searchCurrency
 * @var array $rows
 * @var array $filter
 */
?>

<?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel'=> $searchCurrency,

            'columns' => [

                'id',
                'date',
                'value',
                [
                    'attribute'=>'proportion',
                    'filter'=> ["USD_RUB"=>"USD_RUB","RUB_JPY"=>"RUB_JPY"],
                    //'filter'=> $filter



                ],
             ],
        ]);?>
<?php
//возвращает массив Get объектов;
$dataProvider->getModels();

?>

<table class="table">
    <tr>
        <th>ID</th>
        <th>Proportion</th>
        <th>Value</th>
        <th>Date</th>
    </tr>
    <?php foreach ($rows as $row): ?>
        <tr>
            <td><?= $row->id ?></td>
            <td><?= $row->proportion ?></td>
            <td><?= $row->value ?></td>
            <td><?= $row->date ?></td>
        </tr>
    <?php endforeach; ?>
</table>

<?php
var_dump($filter);
//var_dump($rows);







