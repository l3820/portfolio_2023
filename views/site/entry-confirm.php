<?php
use yii\helpers\Html;
?>
<p>Вы ввели следующую информацию:</p>

<ul>
    <li><label>Login</label>: <?= Html::encode($model->login) ?></li>
    <li><label>Password</label>: <?= Html::encode($model->password) ?></li>
     <li><label>Telefone</label>: <?= Html::encode($model->telephone) ?></li>
</ul>