<?php

namespace app\assets;

use yii\web\AssetBundle;

class VueAsset extends AssetBundle
{
    /*public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'vue/dist/css/chunk-vendors.css',
    ];
    public $js = [
        'vue/dist/js/app.js',
        'vue/dist/js/chunk-vendors.js',
    ];*/

    public $sourcePath = '../vue/dist';
    public $js = ['js/app.js', 'js/chunk-vendors.js'];
    public $css = ['css/app.css', 'css/chunk-vendors.css'];
    public $publishOptions = [
        'forceCopy' => YII_DEBUG
    ];
}