<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%comments}}`.
 */
class m220308_115606_add_reply_id_column_reply_column_to_comments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%comments}}', 'reply_id', $this->integer());
        $this->addColumn('{{%comments}}', 'reply', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%comments}}', 'reply_id');
        $this->dropColumn('{{%comments}}', 'reply');
    }
}
