<?php

use yii\db\Migration;

/**
 * Class m220202_094544_create_currency_rates
 */
class m220202_094544_create_currency_rates extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('currency_rates', [
            'id' => $this->primaryKey(),
            'proportion' => $this->text(),
            'value' => $this->float()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('currency_rates');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220202_094544_create_currency_rates cannot be reverted.\n";

        return false;
    }
    */
}
