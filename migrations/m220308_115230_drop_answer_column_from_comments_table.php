<?php

use yii\db\Migration;

/**
 * Handles dropping columns from table `{{%comments}}`.
 */
class m220308_115230_drop_answer_column_from_comments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%comments}}', 'answer');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%comments}}', 'answer', $this->text());
    }
}
