<?php

use yii\db\Migration;

/**
 * Handles dropping columns from table `{{%product}}`.
 */
class m220728_172248_drop_color_column_from_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%product}}', 'color');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%product}}', 'color', $this->string());
    }
}
