<?php

use yii\db\Migration;

/**
 * Class m220203_181716_add_currency_rates_column
 */
class m220203_181716_add_currency_rates_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('currency_rates','date',$this->timestamp());
        $this->alterColumn('currency_rates','proportion',$this->char(10));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m220203_181716_add_currency_rates_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220203_181716_add_currency_rates_column cannot be reverted.\n";

        return false;
    }
    */
}
