<?php

use yii\db\Migration;

/**
 * Handles dropping columns from table `{{%comments}}`.
 */
class m220308_115430_drop_answer_id_column_from_comments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%comments}}', 'answer_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%comments}}', 'answer_id', $this->integer());
    }
}
