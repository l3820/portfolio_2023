<?php

use yii\db\Migration;

/**
 * Handles dropping columns from table `{{%comments}}`.
 */
class m220311_055550_drop_reply_column_from_comments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%comments}}', 'reply');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%comments}}', 'reply', $this->text());
    }
}
