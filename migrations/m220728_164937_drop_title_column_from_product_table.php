<?php

use yii\db\Migration;

/**
 * Handles dropping columns from table `{{%product}}`.
 */
class m220728_164937_drop_title_column_from_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%product}}', 'title');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%product}}', 'title', $this->varchar());
    }
}
