<?php

use yii\db\Migration;

/**
 * Handles dropping columns from table `{{%order}}`.
 */
class m220728_173622_drop_status_column_from_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%order}}', 'status');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%order}}', 'status', $this->text());
    }
}
