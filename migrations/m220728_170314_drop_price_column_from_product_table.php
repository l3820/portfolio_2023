<?php

use yii\db\Migration;

/**
 * Handles dropping columns from table `{{%product}}`.
 */
class m220728_170314_drop_price_column_from_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%product}}', 'price');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%product}}', 'price', $this->decimal());
    }
}
