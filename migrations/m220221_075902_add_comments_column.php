<?php

use yii\db\Migration;

/**
 * Class m220221_075902_add_comments_column
 */
class m220221_075902_add_comments_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('comments','date',$this->timestamp());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('comments', 'date');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220221_075902_add_comments_column cannot be reverted.\n";

        return false;
    }
    */
}
