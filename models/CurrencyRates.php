<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property string $proportion
 * @property float $value
 * @property string $date
 */
class CurrencyRates extends ActiveRecord
{
    public static function tableName()
    {
        return '{{currency_rates}}';
    }
}
