<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\data\DataFilter;
use yii\db\Query;

class SearchCurrency extends CurrencyRates
{
    public $proportion;

   public function rules()
    {
        return [
            ['proportion', 'safe']
        ];
    }

    public function search()
    {
        $this->load(Yii::$app->request->queryParams);
        $this->validate();

        $query = CurrencyRates::find()
            ->orderBy(['date' => SORT_DESC])
            ->filterWhere(['Proportion' => $this->proportion]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $dataProvider;
    }


}