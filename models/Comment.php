<?php


namespace app\models;

use DateTime;
use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property string $comment
 * @property float $author
 * @property string $date
 * @property int $reply_id
 * @property int $author_id
 */
class Comment extends ActiveRecord
{
    /**
     * @var mixed|null
     */


    public static function tableName()
    {
        return '{{comments}}';
    }

    public function attributeLabels()
    {
        return [
            'comment' => 'Ваш комментарий',

        ];
    }

    public function rules()
    {
        return [
            ['comment', 'required','message'=>'Введите текст комментария.'],
            [['comment'],'string', 'max' => 100,'tooLong'=>'Комментарий превышает 100 символов.'],
            //['reply', 'required','message'=>'Введите текст ответа на комментарий.'],

        ];
    }

    static function getDateDiff($date){

        $now = new DateTime();
        //$new = $now->modify('-1 day');
        return date_diff($now,date_create($date,timezone_open("Europe/Moscow")));

    }

    static function getArrTime($interval)
    {
        if($interval->y > 0) {
            return ['год', 'года', 'лет'];
        }
        if($interval->m > 0){
            return ['месяц','месяца','месяцев'];
        }
        if($interval->d > 0){
            return ['день','дня','дней'];
        }
        if($interval->h > 0){
            return ['час','часа','часов'];
        }
        if($interval->i > 0){
            return ['минута','минуты','минут'];
        }
        if($interval->s > 0){
            return ['секунда','секунды','секунд'];
        }


    }
}
