<?php

namespace app\fakes;

use app\interfaces\UserRepositoryInterface;
use app\models\User;

class FakeUserRepository implements UserRepositoryInterface
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function find(int $id): ?User
    {
        return $this->data;
    }
}