<?php

namespace app\fakes;

use app\interfaces\CommentRepositoryInterface;
use app\models\Comment;

class FakeCommentRepository implements CommentRepositoryInterface
{
    private $data;

    public function __construct($data)
    {

        $this->data = $data;
    }

    public function save(Comment $comment)
    {

    }

    public function getCommentsWithReplies()
    {
        return $this->data;
    }

    public function delete(Comment $comment)
    {

    }
}