<?php

namespace app\interfaces;

interface EmailNotifyServiceInterface
{
    public function send();

}