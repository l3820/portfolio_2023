<?php

namespace app\interfaces;

use app\models\User;

interface UserRepositoryInterface
{
    public function find(int $id):? User;
}
