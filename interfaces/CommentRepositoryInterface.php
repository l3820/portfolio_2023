<?php

namespace app\interfaces;

use app\models\Comment;

interface CommentRepositoryInterface
{
    public function getCommentsWithReplies();

    public function save(Comment $comment);

    public function delete(Comment $comment);
}