<?php

namespace Internetshop\repositories;

use Internetshop\models\Order;

class OrderRepository
{
    public function save(Order $order)
    {
        $order->save();
    }

    public function findNew($userId)
    {
        return Order::findOne(['user_id' => $userId, 'status' => 'new']);
    }

    public function delete(Order $order)
    {
        $order->delete();
    }

    public function getById($orderId)
    {
        return Order::findOne($orderId);
    }
}