<?php

namespace Internetshop\repositories;

use Internetshop\models\OrderItem;

class OrderItemRepository
{
    public function save(OrderItem $orderItem)
    {
        $orderItem->save();
    }

    public function delete(OrderItem $orderItem)
    {
        $orderItem->delete();
    }

    public function count($orderId)
    {
        return OrderItem::find()->where(['order_id'=>$orderId])->count();
    }

    public function getById($orderItemId)
    {
        return OrderItem::findOne($orderItemId);
    }
}