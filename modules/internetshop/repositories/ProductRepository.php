<?php

namespace Internetshop\repositories;

use Internetshop\models\Product;

class ProductRepository
{
    public function save(Product $product)
    {
        $product->save();
    }
}