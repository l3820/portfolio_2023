<?php

namespace Internetshop\controllers;

use DomainException;
use Exception;
use Internetshop\components\BaseApiController;
use Internetshop\models\Order;
use Internetshop\models\OrderItem;
use Internetshop\models\Product;
use Internetshop\services\OrderItemService;
use Internetshop\services\OrderService;
use Internetshop\transformers\OrderTransformer;
use Internetshop\transformers\ProductTransformer;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use Yii;


class OrderController extends BaseApiController
{
    public function actionCart()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        /**
         * @var Order[] $orders
         * @var OrderItem[] $orderItems
         */

        //$orders = Order::findAll(['id'=>27]);
        $orders = Order::findAll(['user_id' => \Yii::$app->user->id, 'status' => Order::STATUS_NEW]);

        $resource = new Collection($orders, new OrderTransformer());

        return (new Manager())->createData($resource)->toArray();

    }

    public function actionPay($orderId)
    {

        $orderService = Yii::$container->get(OrderService::class);

        try {
            $orderService->pay($orderId, Yii::$app->user->id);
        } catch (DomainException $e) {

            Yii::$app->response->statusCode = 404;
            return ['message' => $e->getMessage()];
        } catch (Exception $e) {

            Yii::$app->response->statusCode = 404;
            return ['message' => $e->getMessage()];
        }

        return ['status' => 'success'];

    }

    public function actionHistory()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        /* @var Order[] $orders */

        $orders = Order::find()
            ->where(['and', ['user_id' => \Yii::$app->user->id], ['not', ['status' => Order::STATUS_NEW]]])
            ->all();

        $resource = new Collection($orders, new OrderTransformer());

        return (new Manager())->createData($resource)->toArray();

    }

    public function actionCancel($orderPositionId)
    {

        try {
            Yii::$container->get(OrderService::class)
                ->deleteItem($orderPositionId, Yii::$app->user->id);
        } catch (DomainException $e) {

            Yii::$app->response->statusCode = 404;
            return ['message' => $e->getMessage()];
        } catch (Exception $e) {

            Yii::$app->response->statusCode = 404;
            return ['message' => $e->getMessage()];
        }

        return ['status' => 'success'];

    }

}