<?php

namespace Internetshop\controllers;

use DomainException;
use Exception;
use Internetshop\models\Product;
use Internetshop\models\Order;
use Internetshop\services\OrderService;
use Internetshop\services\ProductService;

use yii\web\Controller;
use Yii;



/**
 * Default controller for the `internetshop` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionList()
    {
        /*$products = Product::find()->all();
        return $this->render('list', ['products' => $products]);*/

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        /**
         * @var Product[] $products
         */
        $products = Product::find()->all();
        $result = [];
        foreach ($products as $product) {
            $result[] = [
                'id' => $product->id,
                'title' => $product->title,
                'status' => $product->status,
                'color' => $product->color,
                'price' => $product->price,
                'description' => $product->description
            ];
        }
        return ["data" => $result];
    }

    public function actionCreate()
    {
        $service = \Yii::$container->get(ProductService::class);
        //$service->addProduct()
    }

    public function actionBuy($productId)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $productService = Yii::$container->get(ProductService::class);

        try {
            $productService->buy($productId, Yii::$app->user->id);
        } catch (DomainException $e) {

            Yii::$app->response->statusCode = 404;
            return ['message' => $e->getMessage()];
        }

        return ['status' => 'success'];

    }

}
