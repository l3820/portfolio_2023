<?php

use Internetshop\models\Product;


/**
 * @var array $products
 */
?>


<div id="product-list">

    <?php foreach ($products as $product): ?>

        <?php /** @var Product $product */ ?>

        <div class="product" data-id="<?= $product->id ?>">
            <b><?= $product->title ?></b>&nbsp<?= $product->price ?>
        </div>

    <? endforeach; ?>

</div>