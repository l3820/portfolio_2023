<?php

namespace Internetshop\dto;

class ProductDto
{
    public $title;
    public $quantity;
    public $price;
    public $color;

}