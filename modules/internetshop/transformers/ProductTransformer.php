<?php

namespace Internetshop\transformers;

use Internetshop\models\Product;
use League\Fractal\TransformerAbstract;

class ProductTransformer extends TransformerAbstract
{
    public function transform(Product $product)
    {
        return [
            'id'=>$product->id,
            'quantity'=>$product->quantity,
            'price'=>$product->price,
            'title'=>$product->title,
            'color'=>$product->color,
            'description'=>$product->description
        ];
    }
}