<?php

namespace Internetshop\transformers;

use DomainException;
use Internetshop\models\Order;
use League\Fractal\TransformerAbstract;
use PHPUnit\Framework\Constraint\IsNull;
use Webmozart\Assert\Assert;
use yii\web\ForbiddenHttpException;

class OrderTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'orderItem'
    ];

    public function transform(Order $order)
    {
        return [
            'id' => $order->id,
            'user_id' => $order->user_id,
            'status' => $order->status
        ];
    }

    public function includeOrderItem(Order $order)
    {
        $orderItems = $order->orderItems;

        return $this->collection($orderItems, new OrderItemTransformer());

    }
}