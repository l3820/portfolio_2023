<?php

namespace Internetshop\transformers;

use Internetshop\models\OrderItem;
use League\Fractal\TransformerAbstract;


class OrderItemTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'product'
    ];

    public function transform(OrderItem $orderItem)
    {
        return [
            'id' => $orderItem->id,
            'product_id' => $orderItem->product_id,
            'quantity' => $orderItem->quantity
        ];
    }

    public function includeProduct(OrderItem $orderItem)
    {
        $product = $orderItem->product;

        return $this->item($product, new ProductTransformer());

    }
}