<?php

namespace Internetshop\models;

use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property int $user_id
 * @property string $status
 * @property OrderItem[] $orderItems
  */

class Order extends ActiveRecord
{
    const STATUS_NEW = 'new';
    const STATUS_BUYED = 'buyed';
    const STATUS_APPROVED = 'approved';
    const STATUS_CANCELED = 'canceled';

    public static function tableName()
    {
        return '{{order}}';
    }

       public function getOrderItems()
    {
        return $this->hasMany(OrderItem::class, ['order_id' => 'id']);
    }
}