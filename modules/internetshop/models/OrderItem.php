<?php

namespace Internetshop\models;

use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property int $order_id
 * @property int $product_id
 * @property int $quantity
 * @property Product $product
 */

class OrderItem extends ActiveRecord
{

    public static function tableName()
    {
        return '{{order_item}}';
    }

    public function getProduct()
    {
        return $this->hasOne(Product::class, ['id' => 'product_id']);
    }

}