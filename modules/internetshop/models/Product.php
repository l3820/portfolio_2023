<?php

namespace Internetshop\models;

use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property string title
 * @property int quantity
 * @property float price
 * @property string color
 * @property string status
 * @property string description
 */

class Product extends ActiveRecord
{
    /**
     * @var mixed|null
     */


    public static function tableName()
    {
        return '{{product}}';
    }

}