<?php

namespace Internetshop\services;

use DomainException;
use Internetshop\models\Order;
use Internetshop\models\OrderItem;
use Internetshop\repositories\OrderItemRepository;
use Internetshop\repositories\OrderRepository;
use Webmozart\Assert\Assert;

class OrderService
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;
    /**
     * @var OrderItemRepository
     */
    private $orderItemRepository;

    public function __construct(OrderRepository $orderRepository, OrderItemRepository $orderItemRepository)
    {
        $this->orderRepository = $orderRepository;

        $this->orderItemRepository = $orderItemRepository;
    }

    public function pay($orderId,$actorUserId)
    {
        $order = Order::findOne($orderId);

        Assert::notNull($order,'Корзина не существует!');

        Assert::eq($actorUserId,$order->user_id,'Доступ запрещен!');

        //Assert::eq($order->status,'new','Заказ не может быть куплен!');

        Assert::eq($order->status, Order::STATUS_NEW,'Заказ не может быть куплен!');

        //$order->status = 'buyed';

        $order->status = Order::STATUS_BUYED;
        $this->orderRepository->save($order);


    }

    public function delete($orderId)
    {
        $order = Order::findOne($orderId);

        $orderItem = OrderItem::find()->where(['order_id'=>$order])->one();

        Assert::notNull($order,'Корзина не существует!');

        Assert::null($orderItem,'В корзине еще есть товары!');

        $this->orderRepository->delete($order);
    }

    public function deleteItem($orderItemId,$actorUserId)
    {
        $orderItem = $this->orderItemRepository->getById($orderItemId);
        Assert::notNull($orderItem,'Такого товара в корзине не существует!');

        $order = $this->orderRepository->getById($orderItem->order_id);
        Assert::notNull($order,'Заказ не найден!');
        Assert::eq($actorUserId,$order->user_id,'Ошибка доступа');
        Assert::eq($order->status, Order::STATUS_NEW,'Ошибка статуса заказа!');

        $this->orderItemRepository->delete($orderItem);

        if ($this->orderItemRepository->count($order->id) <= 0) {
            $this->orderRepository->delete($order);
        }

    }


}