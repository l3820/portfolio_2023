<?php

namespace Internetshop\services;

use DomainException;
use Exception;
use Internetshop\dto\ProductDto;
use Internetshop\models\Order;
use Internetshop\models\OrderItem;
use Internetshop\models\Product;
use Internetshop\repositories\ProductRepository;
use Internetshop\repositories\OrderRepository;
use Internetshop\repositories\OrderItemRepository;
use Yii;

class ProductService
{
    /** @var ProductRepository */
    private $productRepository;

    /** @var OrderRepository  */
    private $orderRepository;

    /** @var OrderItemRepository  */
    private $orderItemRepository;

    public function __construct(
        ProductRepository $productRepository,
        OrderRepository $orderRepository,
        OrderItemRepository $orderItemRepository )
    {
        $this->productRepository = $productRepository;
        $this->orderRepository = $orderRepository;
        $this->orderItemRepository = $orderItemRepository;
    }


    public function addProduct(ProductDto $dto)
    {
        // заполняем Dto->Product->validate->save()

        $product = new Product();

        $product->title = 'Товар №1';
        $product->quantity = 5;
        $product->price = 255.3;
        $product->color = 'Зеленый';
        $product->status = 'Draft';
        $this->productRepository->save($product);

        return $product;

    }

    public function buy($productId,$userId)
    {
        $product = Product::findOne($productId);

        if (!$product) {
            throw new DomainException('Данного товара нет.');
        }

        if ($product->quantity <= 0) {
            throw new DomainException('Товар закончился.');
        }

        $order = $this->orderRepository->findNew($userId);

        if (!$order) {

            $order = new Order();
            $order->user_id = $userId;
            $order->status = Order::STATUS_NEW;
        }

        $orderItem = new OrderItem();

        $tr = Yii::$app->db->beginTransaction();

        try {

            $this->orderRepository->save($order);

            $orderItem->order_id = $order->id;
            $orderItem->product_id = $productId;
            $orderItem->quantity = 1;
            $this->orderItemRepository->save($orderItem);

            $tr->commit();
        } catch (Exception $e) {
            $tr->rollBack();
           throw $e;
        }

        // return $order;

    }

}