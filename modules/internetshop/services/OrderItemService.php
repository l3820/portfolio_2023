<?php

namespace Internetshop\services;

use Internetshop\models\Order;
use Internetshop\models\OrderItem;
use Internetshop\repositories\OrderItemRepository;
use Internetshop\repositories\OrderRepository;
use Webmozart\Assert\Assert;

class OrderItemService
{
    /**
     * @var OrderItemRepository
     */
    private $orderItemRepository;
    private $orderRepository;


    public function __construct(OrderItemRepository $orderItemRepository, OrderRepository $orderRepository)
    {
        $this->orderItemRepository = $orderItemRepository;

        $this->orderRepository = $orderRepository;
    }

}