<?php

namespace Internetshop\components;

use yii\rest\Controller;

abstract class BaseApiController extends Controller
{
    public function beforeAction($action)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return parent::beforeAction($action);
    }
}