<?php

namespace app\repositories;

use app\interfaces\CommentRepositoryInterface;
use app\models\Comment;
use Yii;

class CommentRepository implements CommentRepositoryInterface
{
    public function getCommentsWithReplies()
    {
        /** @var Comment[] $rows */
        $rows = Comment::find()->where(['reply_id' => null])->orderBy(['id' => SORT_DESC])->limit(3)->all();

        /** @var Comment[] $replies */
        $replies = Comment::find()->where(['not', ['reply_id' => null]])->all();

        $arr = [];
        foreach ($rows as $comment) {
            /*$commentObject = ['comment'=>$comment,
                              'replies'=>[]];*/
            $commentReplies = [];
            foreach ($replies as $rep) {
                if ($comment->id == $rep->reply_id) {
                    $commentReplies[] = $rep;
                }
            }
            $arr[] = ['comment' => $comment, 'replies' => $commentReplies];
        }
        return $arr;
    }

    public function save(Comment $comment)
    {
        $comment->save();
    }

    public function delete(Comment $comment)
    {
        $comment->delete();
    }
}