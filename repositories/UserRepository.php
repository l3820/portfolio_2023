<?php

namespace app\repositories;

use app\interfaces\UserRepositoryInterface;
use app\models\User;
use DomainException;
use Yii;

class UserRepository implements UserRepositoryInterface
{

    public function find(int $id): ?User
    {
        return User::findIdentity($id);
    }
}