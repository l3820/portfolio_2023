<?php

namespace app\services;

use app\interfaces\CommentRepositoryInterface;
use app\interfaces\EmailNotifyServiceInterface;
use app\interfaces\UserRepositoryInterface;
use app\models\Comment;
use app\models\User;
use app\repositories\CommentRepository;
use DomainException;
use Yii;

class CommentService
{
    /**
     * @var CommentRepositoryInterface
     * @var EmailNotifyServiceInterface
     */
    private $commentRepository;
    private $emailNotifyService;
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    public function __construct(
        CommentRepositoryInterface $commentRepository,
        EmailNotifyServiceInterface  $emailNotifyService,
        UserRepositoryInterface $userRepository
    ) {
        $this->commentRepository = $commentRepository;
        $this->emailNotifyService = $emailNotifyService;
        $this->userRepository = $userRepository;
    }

    public function delete(Comment $comment, $userId)
    {
        //if ($comment->hasErrors()) {
        if (!$comment->validate()) {
            throw new DomainException('Комментарий содержит ошибки!');
        }

        if (!$this->userRepository->find($userId)) {
            throw new DomainException('Пользователь не найден!');
        }

        if ($userId != $comment->author_id) {
            throw new DomainException('Ошибка доступа!');
        }

        $this->commentRepository->delete($comment);

        return $comment;
    }

    public function update(Comment $comment, $userId)
    {
        //if ($comment->hasErrors()) {
        if (!$comment->validate()) {
            throw new DomainException('Комментарий содержит ошибки!');
        }

        if (!$this->userRepository->find($userId)) {
        //if(!User::findIdentity($userId)){
            throw new DomainException('Пользователь не найден!');
        }

        if ($userId != $comment->author_id) {
            throw new DomainException('Ошибка доступа!');
        }

        $comment->date = date('Y-m-d H:i:s');
        $this->commentRepository->save($comment);

        return $comment;

    }

    public function create(Comment $comment, $userId)
    {
        if (!$comment->validate()) {
            throw new DomainException('Комментарий содержит ошибки!');
        }

        //$user = $this->userRepository->find($userId);
        $user = User::findIdentity($userId);

        if (!$user) {
            throw new DomainException('Пользователь не найден!');
        }


        $comment->author = $user->username;
        $comment->author_id = $userId;
        $comment->date = date('Y-m-d H:i:s');
        $this->commentRepository->save($comment);
        $this->emailNotifyService->send();

        return $comment;
    }
}
