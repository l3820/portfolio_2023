<?php

namespace app\services;

use app\interfaces\EmailNotifyServiceInterface;
use Yii;

class EmailNotifyService implements EmailNotifyServiceInterface
{
    public function send()
    {
        Yii::$app->mailer->compose()
            ->setFrom('ludmila@com')
            ->setTo('user@ru')
            ->setSubject('MyFirstMessage')
            ->setTextBody(date('Y-m-d H:i:s'))
            ->setHtmlBody('<b>HTML</b>')
            ->send();
    }

}
