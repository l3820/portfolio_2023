
    $(document).ready(function () {

    $("#but1").click(function () {
        let lastCommentId = $('#comment-list .comment:last-child').data("id");
        $.ajax({
            url: '/comment/ajax',
            data: {data: lastCommentId},
            dataType: 'json',
            success: function (json) {
                $("#comment-list").append(json.html);

                if (json.isLast) {
                    $("#but1").hide();
                    //alert(json.isLast);
                }
            }
        });

    });

});
