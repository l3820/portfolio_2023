<?php

namespace app\helpers;

use app\models\Comment;
use function Composer\Autoload\includeFile;

class DeclensionHelper
{
    public static function declension($interval, $words)
    {
        $time = '';
        $out = '';

        if ($interval->y > 0) {
            $time = $interval->y;
        } else if ($interval->m > 0) {
            $time = $interval->m;
        } else if ($interval->d > 0) {
            $time = $interval->d;
        } else if ($interval->h > 0) {
            $time = $interval->h;
        } else if ($interval->i > 0) {
            $time = $interval->i;
        } else if ($interval->s > 0) {
            $time = $interval->s;
        }


        if ($time > 19) {
            $out = ($time) % 10;
        } else if ($time < 19) {
            $out = $time;
        }

        switch ($out) :
            case 1:
                $time .= " " . $words[0];
                break;
            case 2:
            case 3:
            case 4:
                $time .= " " . $words[1];
                break;
            default:
                $time .= " " . $words[2];

        endswitch;


        return $time;

    }
}