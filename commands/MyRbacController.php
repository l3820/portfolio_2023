<?php

namespace app\commands;

use Yii;
use yii\base\Exception;
use yii\console\Controller;

/**
 * Инициализатор RBAC выполняется в консоли php yii my-rbac/init
 */
class MyRbacController extends Controller {

    /**
     * @throws Exception
     * @throws \Exception
     */
    public function actionInit() {
        $auth = Yii::$app->authManager;

        $auth->removeAll(); //На всякий случай удаляем старые данные из БД...

        // Создадим роли админа и редактора новостей
        $admin = $auth->createRole('admin');


        // запишем их в БД
        $auth->add($admin);


        // Создаем разрешения. Например, просмотр админки viewAdminPage и редактирование новости updateNews
        $say = $auth->createPermission('say');
        $say->description = 'Приветствие администратора';



        // Запишем эти разрешения в БД
        $auth->add($say);


        // Теперь добавим наследования. Для роли editor мы добавим разрешение updateNews,
        // а для админа добавим наследование от роли editor и еще добавим собственное разрешение viewAdminPage

        // Роли «Редактор новостей» присваиваем разрешение «Редактирование новости»
        $auth->addChild($admin,$say);



        // Назначаем роль admin пользователю с ID 1
        $auth->assign($admin, 100);


    }
}