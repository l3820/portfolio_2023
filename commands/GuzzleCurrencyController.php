<?php

namespace app\commands;

use app\models\CurrencyRates;
use GuzzleHttp\Client;
use yii\console\Controller;

class GuzzleCurrencyController extends Controller
{
    public function actionGuzzle()
    {
        $client = new Client();
        //$res = $client->request('GET', 'https://free.currconv.com/api/v7/convert?q=USD_RUB,USD_EUR&compact=ultra&apiKey=f147752d9f4fc4c904f9');
        $arrResp = [
            'https://free.currconv.com/api/v7/convert?q=USD_RUB,USD_EUR&compact=ultra&apiKey=f147752d9f4fc4c904f9',
            'https://free.currconv.com/api/v7/convert?q=RUB_MXN,USD_MXN&compact=ultra&apiKey=f147752d9f4fc4c904f9',
            'https://free.currconv.com/api/v7/convert?q=RUB_JPY&compact=ultra&apiKey=f147752d9f4fc4c904f9'
        ];

        foreach ($arrResp as $url) {
            $res = $client->request('GET', $url);

            $json = json_decode($res->getBody());
            foreach ($json as $key => $value) {
                $currency = new CurrencyRates();
                $currency->proportion = $key;
                $currency->value = $value;
                $currency->date = date('Y-m-d H:i:s');
                $currency->save();

            }

        }



        //print $json->{'USD_RUB'};
        //echo $res->getBody();
        /*$json = json_decode($res->getBody());
        var_dump($json);
        var_dump($json->USD_RUB);*/

        /*$currency = new CurrencyRates();
        $currency->proportion = 'USD_RUB';
        $currency->value = $json->USD_RUB;
        $currency->date = date('Y-m-d H:i:s');
        $currency->save();*/

    }
}