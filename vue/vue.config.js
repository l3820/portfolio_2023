const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: [
    'vuetify'
  ],
  filenameHashing: false,
  devServer:{proxy:"http://localhost/"}
})
