import Product from "@/pages/Product";
import VueRouter from "vue-router";
import Vue from "vue";
import OrderPage from "@/pages/OrderPage";
import HistoryPage from "@/pages/HistoryPage";

export const routes = [
    { path: '/test', component: Product },
    { path: '/order', component: OrderPage},
    { path: '/history', component: HistoryPage}
]

Vue.use(VueRouter)

export default new VueRouter ({
    routes
})