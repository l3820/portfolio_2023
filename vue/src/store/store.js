import Vue from 'vue'
import Vuex from 'vuex'
import axios from "axios";
Vue.use(Vuex)

// Create a new store instance.
const store = new Vuex.Store({
    state () {
        return {
            cart: [],
            snackbar: { active: false, color: "", message: "", timer: 4000 }
        }
    },
    mutations: {
        setCart (state, payload) {
            state.cart = payload
        },
        UPDATE_SNACKBAR(state, snackbar) {
            state.snackbar = snackbar;
        }

    },
    actions: {
        loadCart(context) {
            axios.get('/internetshop/order/cart').then(response => {
                console.log(response.data.data)
                context.commit("setCart",response.data.data)
            })
        },
        cancelOrderItem(context, orderItemId) {
            try {
                axios.get('/internetshop/order/cancel/', {params: {orderPositionId: orderItemId}}).then(() => {
                    context.dispatch('loadCart')
                })
                console.log('Success')

            } catch (e) {
                console.log('Some error')
            }
        },
        loadHistory(context) {
            axios.get('/internetshop/order/history').then(response => {
                console.log(response.data.data)
                context.commit("setCart",response.data.data)
            })
        },
        showSnack({ commit }, { message, color, timer }) {
            commit("UPDATE_SNACKBAR", {
                active: true,
                color: color,
                message: message,
                timer: timer
            });
        }
    }
})

export default store;
/*
const app = createApp({ /!* your root component *!/ })

// Install the store instance as a plugin
app.use(store)*/
