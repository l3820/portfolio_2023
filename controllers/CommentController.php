<?php

namespace app\controllers;

use app\models\Comment;
use app\repositories\CommentRepository;
use app\helpers\DeclensionHelper;
use app\services\CommentService;
use app\services\EmailNotifyService;
use DateTime;
use DomainException;
use Yii;
use yii\db\Exception;
use yii\db\Query;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotAcceptableHttpException;
use yii\web\NotFoundHttpException;


class CommentController extends Controller
{
    public function actionAjax($data)
    {
        if (!Yii::$app->request->isAjax) {
            throw new NotAcceptableHttpException("Что-то пошло не так...");
        }

        $comments = Comment::find()
            ->where(['and', ['reply_id' => null], ['<', 'id', $data]])
            ->orderBy(['id' => SORT_DESC])
            ->limit(3)
            ->all();

        $replies = Comment::find()
            ->where(['not', ['reply_id' => null]])
            ->all();

        $htmlReplies = '';
        foreach ($comments as $comment) {

            $commentReplies = [];
            foreach ($replies as $rep) {
                if ($comment->id == $rep->reply_id) {
                    $commentReplies[] = $rep;
                }
            }
            $htmlReplies .= $this->renderPartial('_comment', ['comment' => $comment, 'replies' => $commentReplies]);
        }

        $isLast = false;

        $count = Comment::find()
            ->where(['and', ['reply_id' => null], ['<', 'id', $data]])
            ->count();

        if ($count <= 3) {
            $isLast = true;
        }

        return json_encode([
            'html' => $htmlReplies,
            'isLast' => $isLast
        ]);
    }

    public function actionIndex()
    {
        //$commentsRepository = new CommentRepository();
        $commentsRepository = Yii::$container->get(CommentRepository::class);

        $comments = $commentsRepository->getCommentsWithReplies();

        return $this->render('index', ['comments' => $comments]);

    }

    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            throw new ForbiddenHttpException('Необходимо авторизоваться!');
        }
        $comment = new Comment();

        if ($comment->load(Yii::$app->request->post()) && $comment->validate()) {

            $commentService = Yii::$container->get(CommentService::class);

            try {
                $commentService->create($comment, Yii::$app->user->id);
            } catch (DomainException $e) {

                Yii::$app->session->setFlash('danger', $e->getMessage());
                return $this->redirect('index');

            }

            Yii::$app->session->setFlash('success', "Ваш комментарий успешно добавлен!");
            //exit('Ваш комментарий добавлен!');
            return $this->redirect('index');
        }

      /*  $emailNotifyService = Yii::$container->get(EmailNotifyService::class);

        $emailNotifyService->send();*/

        return $this->render('create', ['model' => $comment]);

    }

    public function actionDelete($id)
    {
        $comment = Comment::findOne($id);

        if (!$comment) {
            Yii::$app->session->setFlash('danger', "Выбранной записи в таблице не существует!");
            return $this->redirect('index');
        }

        /*if (Yii::$app->user->id != $comment->author) {

            Yii::$app->session->setFlash('danger', "Ошибка доступа");
            return $this->redirect('index');
        }*/

        //$comment->delete();

        $commentService = Yii::$container->get(CommentService::class);

        try {
            $commentService->delete($comment, Yii::$app->user->id);
        } catch (DomainException $e) {

            Yii::$app->session->setFlash('danger', $e->getMessage());
            return $this->redirect('index');

        }

        Yii::$app->session->setFlash('success', "Комментарий удален!");
        return $this->redirect('index');

    }

    public function actionUpdate($id)
    {
        if (Yii::$app->user->isGuest) {
            throw new ForbiddenHttpException('Необходимо авторизоваться!');
        }

        $comment = Comment::findOne($id);
        if (!$comment) {
            throw new NotFoundHttpException();
        }
        //существует ли такой пользователь?
        //добавить,что автор-тек.пользователь.
        //"Коммент не найден и у вас нет прав".
        //Добавит flesh-сообщение,что коммент добавлен.

        //if (Yii::$app->user->id != $comment->author) {
        /*if (Yii::$app->user->identity->username != $comment->author) {
            Yii::$app->session->setFlash('danger', "Ошибка доступа");
            return $this->redirect('index');
        }*/

        if ($comment->load(Yii::$app->request->post()) && $comment->validate()) {

            $commentService = Yii::$container->get(CommentService::class);

            try {
                $commentService->update($comment, Yii::$app->user->id);
            } catch (DomainException $e) {

                Yii::$app->session->setFlash('danger', $e->getMessage());
                return $this->redirect('index');

            }

            /*$comment->date = date('Y-m-d H:i:s');
            $comment->save();*/

            Yii::$app->session->setFlash('success', "Комментарий успешно обновлен!");
            return $this->redirect('index');
        }

        return $this->render('update', ['model' => $comment]);
    }

    public function actionReply($id)
    {
        if (Yii::$app->user->isGuest) {
            throw new ForbiddenHttpException('Необходимо авторизоваться!');
        }

        $comment = Comment::findOne($id);
        if (!$comment) {
            throw new NotFoundHttpException();
        }

        $reply = new Comment();

        if ($reply->load(Yii::$app->request->post()) && $reply->validate()) {

            $reply->reply_id = $id;
            $reply->author = Yii::$app->user->identity->username;
            $reply->date = date('Y-m-d H:i:s');
            $reply->save();

            Yii::$app->session->setFlash('success', "Вы ответили на комментарий!");
            //exit('Ваш комментарий добавлен!');
            return $this->redirect('index');
        }

        return $this->render('reply', ['comment' => $comment, 'reply' => $reply]);


    }


}