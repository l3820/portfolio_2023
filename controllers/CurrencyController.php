<?php

namespace app\controllers;

use app\models\CurrencyRates;
use app\models\SearchCurrency;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\data\DataFilter;

class CurrencyController extends Controller
{
    public function actionIndex()
    {
        $searchCurrency = new SearchCurrency();
        $dataProvider = $searchCurrency->search();

        $rows = CurrencyRates::find()->all();
        $filter = CurrencyRates::find()
            ->asArray()
            ->select('proportion')
            ->distinct()
            ->column();


        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchCurrency' => $searchCurrency,
            'rows' => $rows,
            'filter' => $filter
        ]);

    }

       public function actionSay($message = 'Здравствуйте, admin!')
    {

        return $this->render('say', ['message' => $message]);
    }



}